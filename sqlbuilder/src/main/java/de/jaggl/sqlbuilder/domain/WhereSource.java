package de.jaggl.sqlbuilder.domain;

public interface WhereSource
{
    String build();
}