package de.jaggl.sqlbuilder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Join implements SqlFragmentSource
{
    private String table, matching;
    private JoinType joinType;

    public Join(String table, String matching)
    {
        this(table, matching, null);
    }

    public Join(String table, JoinType joinType)
    {
        this(table, null, joinType);
    }

    public Join(String table)
    {
        this(table, null, null);
    }
}