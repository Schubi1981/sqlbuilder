package de.jaggl.sqlbuilder.domain;

import java.util.Collection;

import de.jaggl.sqlbuilder.builders.SqlBuilder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Set implements SqlFragmentSource
{
    private String column;
    private Object value;
    private SqlBuilder sqlBuilder;
    private String[] columns;

    public Set(String column, Object value)
    {
        this.column = column;
        this.value = value;
    }

    public Set(SqlBuilder sqlBuilder, String... columns)
    {
        this.sqlBuilder = sqlBuilder;
        this.columns = columns;
    }

    public Set(SqlBuilder sqlBuilder, Collection<String> columns)
    {
        this.sqlBuilder = sqlBuilder;
        this.columns = columns.toArray(new String[0]);
    }
}