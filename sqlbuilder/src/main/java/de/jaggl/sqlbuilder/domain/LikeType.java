package de.jaggl.sqlbuilder.domain;

public enum LikeType
{
    BEFORE, AFTER, BOTH;
}