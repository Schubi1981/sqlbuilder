package de.jaggl.sqlbuilder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class OrderBy implements SqlFragmentSource
{
    private String column;
    private OrderDir orderDir;

    public OrderBy(String column)
    {
        this(column, null);
    }
}