package de.jaggl.sqlbuilder.domain;

import java.util.Collection;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Select implements SqlFragmentSource
{
    private String[] fields;

    public Select(String... fields)
    {
        this.fields = fields;
    }

    public Select(Collection<String> fields)
    {
        this.fields = fields.toArray(new String[0]);
    }
}