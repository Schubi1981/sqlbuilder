package de.jaggl.sqlbuilder.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class Unquoted
{
    private String unquoted;

    public static Unquoted unquoted(String unquoted)
    {
        return new Unquoted(unquoted);
    }

    @Override
    public String toString()
    {
        return unquoted;
    }
}