package de.jaggl.sqlbuilder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Distinct implements SqlFragmentSource
{
    private @Getter boolean distinct = true;
}