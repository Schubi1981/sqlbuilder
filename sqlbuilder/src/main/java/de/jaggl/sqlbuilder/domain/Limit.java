package de.jaggl.sqlbuilder.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Limit implements SqlFragmentSource
{
    private @Getter int size;
    private int offset;

    public Limit(int size)
    {
        this(size, 0);
    }

    public int getOffset(String syntax)
    {
        if (syntax.equals(Syntax.SYBASE))
        {
            return offset + 1;
        }
        return offset;
    }

    public boolean hasOffset()
    {
        return offset > 0;
    }
}