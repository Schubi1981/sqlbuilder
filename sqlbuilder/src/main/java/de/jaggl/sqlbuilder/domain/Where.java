package de.jaggl.sqlbuilder.domain;

import static de.jaggl.sqlbuilder.domain.Combination.AND;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Where implements SqlFragmentSource
{
    private String column;
    private Object value;
    private Combination combination;
    private WhereSource whereSource;

    public Where(String column, Object value, Combination combination)
    {
        this.column = column;
        this.value = value;
        this.combination = combination;
    }

    public Where(String column, Object value)
    {
        this(column, value, AND);
    }

    public Where(String statement)
    {
        this(statement, null, AND);
    }

    public Where(String statement, Combination combination)
    {
        this(statement, null, combination);
    }

    public Where(WhereSource whereSource, Combination combination)
    {
        this.whereSource = whereSource;
        this.combination = combination;
    }

    public Where(WhereSource whereSource)
    {
        this(whereSource, AND);
    }
}