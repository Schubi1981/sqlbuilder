package de.jaggl.sqlbuilder.domain;

public enum StatementType
{
    SELECT, INSERT, UPDATE, DELETE;
}