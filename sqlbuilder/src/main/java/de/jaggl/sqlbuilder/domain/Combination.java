package de.jaggl.sqlbuilder.domain;

public enum Combination
{
    AND, OR;
}