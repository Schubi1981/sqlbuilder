package de.jaggl.sqlbuilder.domain;

public enum OrderDir
{
    ASC, DESC;
}