package de.jaggl.sqlbuilder.domain;

import java.util.Collection;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GroupBy implements SqlFragmentSource
{
    private String[] fields;

    public GroupBy(String... fields)
    {
        this.fields = fields;
    }

    public GroupBy(Collection<String> fields)
    {
        this.fields = fields.toArray(new String[0]);
    }
}