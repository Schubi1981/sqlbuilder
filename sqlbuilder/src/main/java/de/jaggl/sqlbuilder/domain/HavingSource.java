package de.jaggl.sqlbuilder.domain;

public interface HavingSource
{
    String build();
}