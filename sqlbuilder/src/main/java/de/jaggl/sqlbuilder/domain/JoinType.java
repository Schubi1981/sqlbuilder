package de.jaggl.sqlbuilder.domain;

public enum JoinType
{
    LEFT, RIGHT, INNER, OUTER;
}