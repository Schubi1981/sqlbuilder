package de.jaggl.sqlbuilder.domain;

import java.util.Collection;

import de.jaggl.sqlbuilder.builders.SqlBuilder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class From implements SqlFragmentSource
{
    private String[] tables;
    private SqlBuilder sqlBuilder;
    private String alias;

    public From(String... tables)
    {
        this.tables = tables;
    }

    public From(Collection<String> tables)
    {
        this.tables = tables.toArray(new String[0]);
    }

    public From(SqlBuilder sqlBuilder, String alias)
    {
        this.sqlBuilder = sqlBuilder;
        this.alias = alias;
    }
}