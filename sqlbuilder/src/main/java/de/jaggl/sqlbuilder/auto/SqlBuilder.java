package de.jaggl.sqlbuilder.auto;

import static de.jaggl.sqlbuilder.auto.QueryType.EQUALS;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(FIELD)
public @interface SqlBuilder
{
    /**
     * name of the column
     */
    String value();

    QueryType queryType() default EQUALS;
}