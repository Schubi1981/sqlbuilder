package de.jaggl.sqlbuilder.auto;

import static de.jaggl.sqlbuilder.auto.QueryType.EQUALS;
import static de.jaggl.sqlbuilder.auto.QueryType.IS_NOT_NULL;
import static de.jaggl.sqlbuilder.auto.QueryType.IS_NULL;
import static de.jaggl.sqlbuilder.auto.QueryType.LIKE;
import static de.jaggl.sqlbuilder.auto.QueryType.LIKE_AFTER;
import static de.jaggl.sqlbuilder.auto.QueryType.LIKE_BEFORE;
import static de.jaggl.sqlbuilder.auto.QueryType.LIKE_BOTH;
import static de.jaggl.sqlbuilder.auto.QueryType.NOT_EQUALS;
import static de.jaggl.sqlbuilder.auto.QueryType.NOT_LIKE;
import static de.jaggl.sqlbuilder.auto.QueryType.NOT_LIKE_AFTER;
import static de.jaggl.sqlbuilder.auto.QueryType.NOT_LIKE_BEFORE;
import static de.jaggl.sqlbuilder.auto.QueryType.NOT_LIKE_BOTH;
import static de.jaggl.sqlbuilder.auto.QueryType.WHERE_IN;
import static de.jaggl.sqlbuilder.auto.QueryType.WHERE_NOT_IN;
import static de.jaggl.sqlbuilder.builders.SqlBuilder.where;
import static de.jaggl.sqlbuilder.builders.SqlBuilder.whereIn;
import static de.jaggl.sqlbuilder.builders.SqlBuilder.whereLike;
import static de.jaggl.sqlbuilder.builders.SqlBuilder.whereNotIn;
import static de.jaggl.sqlbuilder.builders.SqlBuilder.whereNotLike;

import java.lang.reflect.Field;
import java.util.Collection;

import de.jaggl.sqlbuilder.domain.LikeType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SqlAutoBuilder
{
    public static void appendForObject(de.jaggl.sqlbuilder.builders.SqlBuilder sqlBuilder, Object object)
    {
        try
        {
            Field[] fields = object.getClass().getDeclaredFields();
            for (Field field : fields)
            {
                SqlBuilder anno = field.getAnnotation(SqlBuilder.class);
                if (anno != null)
                {
                    field.setAccessible(true);
                    Object value = field.get(object);
                    if (value != null)
                    {
                        String column = anno.value();
                        QueryType queryType = anno.queryType();
                        if (queryType == EQUALS)
                        {
                            sqlBuilder.append(where(column + " =", value));
                        }
                        if (queryType == NOT_EQUALS)
                        {
                            sqlBuilder.append(where(column + " !=", value));
                        }
                        if (queryType == LIKE)
                        {
                            sqlBuilder.append(whereLike(column, value.toString()));
                        }
                        if (queryType == LIKE_BEFORE)
                        {
                            sqlBuilder.append(whereLike(column, value.toString(), LikeType.BEFORE));
                        }
                        if (queryType == LIKE_AFTER)
                        {
                            sqlBuilder.append(whereLike(column, value.toString(), LikeType.AFTER));
                        }
                        if (queryType == LIKE_BOTH)
                        {
                            sqlBuilder.append(whereLike(column, value.toString(), LikeType.BOTH));
                        }
                        if (queryType == NOT_LIKE)
                        {
                            sqlBuilder.append(whereNotLike(column, value.toString()));
                        }
                        if (queryType == NOT_LIKE_BEFORE)
                        {
                            sqlBuilder.append(whereNotLike(column, value.toString(), LikeType.BEFORE));
                        }
                        if (queryType == NOT_LIKE_AFTER)
                        {
                            sqlBuilder.append(whereNotLike(column, value.toString(), LikeType.AFTER));
                        }
                        if (queryType == NOT_LIKE_BOTH)
                        {
                            sqlBuilder.append(whereNotLike(column, value.toString(), LikeType.BOTH));
                        }
                        if (queryType == WHERE_IN)
                        {
                            if (Collection.class.isAssignableFrom(value.getClass()))
                            {
                                Collection<?> collection = (Collection<?>) value;
                                if (!collection.isEmpty())
                                {
                                    sqlBuilder.append(whereIn(column, collection));
                                }
                            }
                            else
                            {
                                log.warn("queryType 'WHERE_IN' is only allowed for Collections, no sql will be appended (class: " + object.getClass().getName()
                                    + ", field: " + field.getName() + ", type: " + field.getType() + ")");
                            }
                        }
                        if (queryType == WHERE_NOT_IN)
                        {
                            if (Collection.class.isAssignableFrom(value.getClass()))
                            {
                                Collection<?> collection = (Collection<?>) value;
                                if (!collection.isEmpty())
                                {
                                    sqlBuilder.append(whereNotIn(column, collection));
                                }
                            }
                            else
                            {
                                log.warn(
                                    "queryType 'WHERE_NOT_IN' is only allowed for Collections, no sql will be appended (class: " + object.getClass().getName()
                                        + ", field: " + field.getName() + ", type: " + field.getType() + ")");
                            }
                        }
                        if (queryType == IS_NULL)
                        {
                            if (Boolean.class.isAssignableFrom(value.getClass()))
                            {
                                Boolean bool = (Boolean) value;
                                sqlBuilder.append(where(column + " " + (bool.booleanValue() ? "IS NULL" : "IS NOT NULL")));
                            }
                            else
                            {
                                log.warn("queryType 'IS_NULL' is only allowed for Booleans, no sql will be appended (class: " + object.getClass().getName()
                                    + ", field: " + field.getName() + ", type: " + field.getType() + ")");
                            }
                        }
                        if (queryType == IS_NOT_NULL)
                        {
                            if (Boolean.class.isAssignableFrom(value.getClass()))
                            {
                                Boolean bool = (Boolean) value;
                                sqlBuilder.append(where(column + " " + (bool.booleanValue() ? "IS NOT NULL" : "IS NULL")));
                            }
                            else
                            {
                                log.warn("queryType 'IS_NULL' is only allowed for Booleans, no sql will be appended (class: " + object.getClass().getName()
                                    + ", field: " + field.getName() + ", type: " + field.getType() + ")");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("could not auto build sql for " + object, e);
            throw new RuntimeException(e);
        }
    }
}