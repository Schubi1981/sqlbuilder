package de.jaggl.sqlbuilder.auto;

public enum QueryType
{
    EQUALS, NOT_EQUALS, LIKE, LIKE_BEFORE, LIKE_AFTER, LIKE_BOTH, NOT_LIKE, NOT_LIKE_BEFORE, NOT_LIKE_AFTER, NOT_LIKE_BOTH,
    /**
     * only allowed for Collection.class or extensions
     */
    WHERE_IN,
    /**
     * only allowed for Collection.class or extensions
     */
    WHERE_NOT_IN,
    /**
     * only allowed for Boolean.class or extensions
     */
    IS_NULL,
    /**
     * only allowed for Boolean.class or extensions
     */
    IS_NOT_NULL;
}