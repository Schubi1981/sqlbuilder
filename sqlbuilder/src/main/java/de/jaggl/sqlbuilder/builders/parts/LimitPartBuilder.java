package de.jaggl.sqlbuilder.builders.parts;

import java.util.Collection;
import java.util.Map;

import de.jaggl.sqlbuilder.domain.Limit;
import de.jaggl.sqlbuilder.domain.StatementType;
import lombok.ToString;

@ToString(callSuper = true)
public class LimitPartBuilder extends PartBuilder<Limit>
{
    private LimitPartBuilder(Collection<Limit> limits)
    {
        super(limits);
    }

    private LimitPartBuilder(Limit limit)
    {
        super(limit);
    }

    public static LimitPartBuilder limit(int size)
    {
        return new LimitPartBuilder(new Limit(size));
    }

    public static LimitPartBuilder limit(int size, int offset)
    {
        return new LimitPartBuilder(new Limit(size, offset));
    }

    @Override
    public void putReplacements(Map<String, String> replacements, String syntax, StatementType statementType)
    {
        if (hasElements())
        {
            Limit limit = getEntry();
            if (limit.getSize() > 0)
            {
                replacements.put("size", String.valueOf(limit.getSize()));
                if (limit.hasOffset())
                {
                    replacements.put("offsetSize", String.valueOf(limit.getOffset(syntax)));
                }
            }
        }
    }

    @Override
    public boolean isOverridingPartBuilder()
    {
        return true;
    }
}