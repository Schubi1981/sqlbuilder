package de.jaggl.sqlbuilder.builders.fragments;

import static de.jaggl.sqlbuilder.domain.LikeType.AFTER;
import static de.jaggl.sqlbuilder.domain.LikeType.BEFORE;
import static de.jaggl.sqlbuilder.domain.LikeType.BOTH;

import de.jaggl.sqlbuilder.domain.HavingSource;
import de.jaggl.sqlbuilder.domain.LikeType;
import de.jaggl.sqlbuilder.domain.WhereSource;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@RequiredArgsConstructor
public class NotLikeBuilder implements WhereSource, HavingSource
{
    private final String column, value;
    private LikeType likeType;

    @Override
    public String build()
    {
        StringBuilder builder = new StringBuilder(column).append(" NOT LIKE '");
        if ((likeType == BEFORE) || (likeType == BOTH))
        {
            builder.append("%");
        }
        builder.append(value);
        if ((likeType == AFTER) || (likeType == BOTH))
        {
            builder.append("%");
        }
        return builder.append("'").toString();
    }
}