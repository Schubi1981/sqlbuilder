package de.jaggl.sqlbuilder.builders.fragments;

import static de.jaggl.sqlbuilder.helpers.BuilderHelper.quoteIfNeeded;

import de.jaggl.sqlbuilder.domain.HavingSource;
import de.jaggl.sqlbuilder.domain.WhereSource;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BetweenBuilder implements WhereSource, HavingSource
{
    private String column;
    private Object min, max;

    @Override
    public String build()
    {
        return new StringBuilder(column)
            .append(" BETWEEN ")
            .append(quoteIfNeeded(min))
            .append(" AND ")
            .append(quoteIfNeeded(max)).toString();
    }
}