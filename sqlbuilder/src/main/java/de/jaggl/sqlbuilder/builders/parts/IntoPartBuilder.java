package de.jaggl.sqlbuilder.builders.parts;

import java.util.Collection;
import java.util.Map;

import de.jaggl.sqlbuilder.domain.Into;
import de.jaggl.sqlbuilder.domain.StatementType;
import lombok.ToString;

@ToString(callSuper = true)
public class IntoPartBuilder extends PartBuilder<Into>
{
    private IntoPartBuilder(Collection<Into> intos)
    {
        super(intos);
    }

    private IntoPartBuilder(Into into)
    {
        super(into);
    }

    public static IntoPartBuilder into(String tableName)
    {
        return new IntoPartBuilder(new Into(tableName));
    }

    @Override
    public void putReplacements(Map<String, String> replacements, String syntax, StatementType statementType)
    {
        if (hasElements())
        {
            Into into = getEntry();
            replacements.put("table", into.getTable());
        }
    }

    @Override
    public boolean isOverridingPartBuilder()
    {
        return true;
    }
}