package de.jaggl.sqlbuilder.builders.parts;

import static java.util.Arrays.asList;

import java.util.Collection;

import de.jaggl.sqlbuilder.domain.ReplacementsSource;
import de.jaggl.sqlbuilder.domain.SqlFragmentSource;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public abstract class PartBuilder<T extends SqlFragmentSource> implements ReplacementsSource
{
    protected Collection<T> data;

    @SuppressWarnings("unchecked")
    protected T getEntry()
    {
        return (T) data.toArray()[0];
    }

    public abstract boolean isOverridingPartBuilder();

    public PartBuilder(T data)
    {
        this.data = asList(data);
    }

    public boolean hasElements()
    {
        return !data.isEmpty();
    }
}