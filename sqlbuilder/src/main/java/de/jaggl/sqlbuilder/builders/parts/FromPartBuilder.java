package de.jaggl.sqlbuilder.builders.parts;

import static de.jaggl.sqlbuilder.helpers.BuilderHelper.embrace;
import static de.jaggl.sqlbuilder.helpers.ListHelper.implodeArray;

import java.util.Collection;
import java.util.Map;

import de.jaggl.sqlbuilder.builders.SqlBuilder;
import de.jaggl.sqlbuilder.domain.From;
import de.jaggl.sqlbuilder.domain.StatementType;
import lombok.ToString;

@ToString(callSuper = true)
public class FromPartBuilder extends PartBuilder<From>
{
    private FromPartBuilder(Collection<From> froms)
    {
        super(froms);
    }

    private FromPartBuilder(From from)
    {
        super(from);
    }

    public static FromPartBuilder from(String... tableNames)
    {
        return new FromPartBuilder(new From(tableNames));
    }

    public static FromPartBuilder from(Collection<String> tableNames)
    {
        return new FromPartBuilder(new From(tableNames));
    }

    public static FromPartBuilder from(SqlBuilder sqlBuilder, String alias)
    {
        return new FromPartBuilder(new From(sqlBuilder, alias));
    }

    @Override
    public void putReplacements(Map<String, String> replacements, String syntax, StatementType statementType)
    {
        if (hasElements())
        {
            replacements.put("table", buildTable(syntax));
        }
    }

    @Override
    public boolean isOverridingPartBuilder()
    {
        return false;
    }

    private String buildTable(String syntax)
    {
        StringBuilder builder = new StringBuilder();
        for (From from : data)
        {
            if (builder.length() > 0)
            {
                builder.append(", ");
            }
            if (from.getSqlBuilder() != null)
            {
                builder
                    .append(embrace(from.getSqlBuilder().buildSelect(syntax)))
                    .append(" AS ")
                    .append(from.getAlias());
            }
            else
            {
                builder.append(implodeArray(", ", from.getTables()));
            }
        }
        return builder.toString();
    }
}